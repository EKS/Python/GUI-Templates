#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This template is meant as starting point for desktop apps using modern TKinter.

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import os
import re
import tkinter as tk
from tkinter import ttk as ttk
from tkinter import messagebox
from tkinter import filedialog
"""
tkinter.filedialog.asksaveasfilename()
tkinter.filedialog.asksaveasfile()
tkinter.filedialog.askopenfilename()
tkinter.filedialog.askopenfile()
tkinter.filedialog.askdirectory()
tkinter.filedialog.askopenfilenames()
tkinter.filedialog.askopenfiles()
"""
main_window = None


def main_file_open_dialog(*args):
    """Display file open dialog."""
    filename = filedialog.askopenfilename()
    messagebox.showinfo(title='Info', message='File selected.', detail=f'{filename}')
    pass


def main_file_save(*args):
    """Save file."""
    messagebox.showwarning(title='Save file', message='File was not saved.', detail='Function not implemented.')
    pass


def main_file_save_as_dialog(*args):
    """Save file as dialog."""
    filename = filedialog.asksaveasfilename()
    messagebox.showwarning(title='Warning', message='File not saved.', detail=f'{filename}')
    pass


def main_about(*args):
    """Display about dialog."""
    messagebox.showinfo(title='About Dialog',
                        message='This template is meant as starting point for desktop apps using modern TKinter.',
                        detail='Lizenziert unter EUPL V. 1.2\
    Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\
    Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\
    eMail: H.Brand@gsi.de\
    Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme')
    pass


def main_close_dialog(*args):
    """Display close dialog."""
    if messagebox.askyesno(title='Quit Application?', icon='question', message='Are you sure to quit this application?'):
        print('Application stopped by user. (Close Window or Menu>File>quit)')
        exit(0)
    pass


def main_exit_dialog(*args):
    """Display exit diaFalselog."""
    if messagebox.askyesno(title='Exit Application?', icon='question', message='Are you sure to exit this application?'):
        print('Application stopped by user. (Menu>File>Exit)')
        exit(0)
    pass


def main_show_help(*args):
    """Display help text in dialog window."""
    modal = True
    help_window = tk.Toplevel(main_window)
    help_window.title('Main Help Window')
    help_window.transient(main_window)
    help_window.resizable(True, True)
    # help_window.geometry('640x480+0+0')  # Set default size
    # help_window.minsize(320, 240)  # Make comment if not desired.
    # help_window.maxsize(1920, 1080)  # Full HD. Make comment if not desired.

    def dismiss():
        help_window.grab_release()
        help_window.destroy()
        pass
    help_window.protocol('WM_DELETE_WINDOW', dismiss)
    # Configure content
    help_window.columnconfigure(0, weight=1)
    help_window.rowconfigure(0, weight=1)
    help_frame = ttk.Frame(help_window, padding='3 3 12 12')
    help_frame.grid(column=0, row=0)  # , sticky=(N, W, E, S))
    # Configure main_frame content
    ttk.Label(help_frame, text='This is the help text for this application.').grid(column=0, row=0, sticky=(tk.N, tk.W))
    help_text = """This script serves as templalte for new GUIs."""
    ttk.Label(help_frame, text=help_text).grid(column=0, row=1, sticky=(tk.N, tk.W))
    ttk.Button(help_frame, text='Done', command=dismiss).grid(column=0, row=3, sticky=(tk.S, tk.W))
    for child in help_frame.winfo_children():
        child.grid_configure(padx=5, pady=5)
    help_window.wait_visibility()
    if modal:  # Make help window modal
        help_window.grab_set()
        help_window.wait_window()
        help_window.grab_release()
    pass


def main_window_configure():
    """Configure the main window."""
    main_window.title(re.split('.py', os.path.basename(__file__))[0] + ' Main Window')
    main_window.protocol('WM_DELETE_WINDOW', main_close_dialog)
    main_window.resizable(True, True)
    if False:
        main_window.attributes('-fullscreen', 1)
    else:
        main_window.geometry('640x480+0+0')  # Set default size
        main_window.minsize(320, 240)  # Make comment if not desired.
        main_window.maxsize(1920, 1080)  # Full HD. Make comment if not desired.
    main_window.columnconfigure(0, weight=1)
    main_window.rowconfigure(0, weight=1)
    pass


def main_menu_configure():
    """Configure the main menu bar."""
    main_window.option_add('*tearOff', False)  # Eliminate 'tear-off' menus
    main_menu_bar = tk.Menu(main_window)
    main_window['menu'] = main_menu_bar
    # Add menu items
    main_menu_file = tk.Menu(main_menu_bar)
    main_menu_help = tk.Menu(main_menu_bar)
    main_menu_bar.add_cascade(menu=main_menu_file, label='File', underline=0)
    main_menu_bar.add_cascade(menu=main_menu_help, label='Help')
    # Add sub-menu file
    main_menu_file.add_command(label='New',)
    main_menu_file.add_command(label='Open...', accelerator='Ctrl+o', command=main_file_open_dialog)
    main_menu_file.add_command(label='Save', accelerator='Ctrl+s', command=main_file_save)
    main_menu_file.add_command(label='Save as...', command=main_file_save_as_dialog)
    main_menu_file.add_separator()
    main_menu_file.add_command(label='Quit', accelerator='Ctrl+q', command=main_close_dialog)
    main_menu_file.add_command(label='Exit', accelerator='Ctrl+x', command=main_exit_dialog)
    # Add sub-menu help
    main_menu_help.add_command(label='Help...', command=main_show_help)
    main_menu_help.add_separator()
    main_menu_help.add_command(label='About...', command=main_about)
    # Set sub-menu item states
    main_menu_file.entryconfigure('New', state=tk.DISABLED)
    # main_menu_help.entryconfigure('Help...', state=tk.DISABLED)
    # Bind keyboard short-cuts
    main_window.bind_all('<Control-o>', main_file_open_dialog)
    main_window.bind_all('<Control-s>', main_file_save)
    main_window.bind_all('<Control-q>', main_close_dialog)
    main_window.bind_all('<Control-x>', main_exit_dialog)
    pass


def main_frame_configure():
    """Configure the main frame within toplevel main window."""
    main_frame = ttk.Frame(main_window, padding='3 3 12 12')
    main_frame.grid(column=0, row=0)  # , sticky=(N, W, E, S))
    # Configure main_frame content
    ttk.Label(main_frame, text='This is the applications main_frame.').grid(column=0, row=0, sticky=(tk.N, tk.W))
    for child in main_frame.winfo_children():
        child.grid_configure(padx=5, pady=5)
    pass


def main():
    """This is the main entry point for this application."""
    global main_window
    main_window = tk.Tk()
    # windowing_system = main_window.tk.call('tk', 'windowingsystem')  # possibly returns x11, win32 or aqua
    # print(f'Windows system: {windowing_system}')
    main_window_configure()  # Configure the toplevel main window
    main_menu_configure()  # Configure the main menu bar.
    main_frame_configure()  # Configure the main frame within toplevel main window.
    main_window.mainloop()  # Start tk event loop.
    pass


if __name__ == '__main__':
    print(os.path.basename(__file__) + "\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    main()
    print(__name__, 'Done.')
