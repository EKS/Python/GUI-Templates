print("<program>\n\
    Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
    This program comes with ABSOLUTELY NO WARRANTY.\n\
    This is free software, and you are welcome to\n\
    redistribute it under certain conditions.")