#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This template is meant as starting point for desktop apps using DearPyGui.

For more information on DearPyGUI refer to
- https://pypi.org/project/dearpygui/1.4.0/
- https://dearpygui.readthedocs.io/en/latest/
- https://github.com/hoffstadt/DearPyGui
- https://github.com/hoffstadt/DearPyGui_Ext

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
"""
import dearpygui.dearpygui as dpg
import dearpygui._dearpygui as internal_dpg
import dearpygui_ext.themes as dpg_themes  # create_theme_imgui_dark, create_theme_imgui_light
import json
import os
import re


def menu_file_dialog_cancel_cb(sender, app_data, user_data) -> None:
    """Cancel-Callback from menu_file_dialog_cp"""
    # print('Sender: ', sender)
    # print('App Data: ', app_data)
    # print("User Data: ", user_data)
    # print('File dialog canceled by user.')
    pass


def menu_file_dialog_ok_cb(sender, app_data, user_data) -> None:
    """Ok-Callback from menu_file_dialog_cp.

    Selected file are stored in dpg variable registry.
    """
    print('Sender: ', sender)
    print('App Data: ', app_data)
    print(f'Selections(dict): {app_data["selections"]}')
    dpg.set_value('file_selections', json.dumps(app_data['selections']))
    dpg.set_value('file_name', list(app_data['selections'].keys())[0])
    print(f'file_name: {dpg.get_value("file_name")}')
    print(f'file_selections(json): {dpg.get_value("file_selections")}')
    print(f'file_selections(dict from json): {json.loads(dpg.get_value("file_selections"))}')
    pass


def menu_file_save_cb(sender, app_data, user_data) -> None:
    """Callback from menu-file-save.

    Pre-seleted file is saved.
    """
    print("Sender: ", sender)
    print("App Data: ", app_data)
    print("User Data: ", user_data)
    file_name = dpg.get_value('file_name')
    try:
        file_selections_dict = json.loads(dpg.get_value("file_selections"))
        print(f'Files to save(file_selections(dict from json)): {file_selections_dict}')
    except Exception:
        file_selections_dict = None
    print(f'file_name: {file_name}')
    print('WARNING Nothing saved. Function not implemented.')
    pass


def menu_file_dialog_cp(sender) -> None:
    """Callback from menu-file-open or menu-file-open_save_as.

    Shows the standard file dialogue window.
    """
    # print(f'Menu Item: {sender}')
    with dpg.file_dialog(label='Select file(s) ...',
                         modal=True, directory_selector=False,
                         default_path='.', default_filename='',
                         callback=menu_file_dialog_ok_cb, cancel_callback=menu_file_dialog_cancel_cb,
                         width=550, height=400, show=True):
        dpg.add_file_extension(".*")
        dpg.add_file_extension(".py", color=(150, 255, 150, 255))
        # dpg.add_file_extension("Source files (*.py){.py}", color=(0, 255, 255, 255))
        # dpg.add_file_extension(".py", color=(0, 255, 255, 255), custom_text="[Python]")
    pass


def menu_file_save_session_cb(sender, app_data, user_data) -> None:
    """Callback from menu-file-session.

    Save session settings to ini-file.
    """
    dpg.save_init_file(user_data)


def menu_settings_font_cb() -> None:
    """Callback from menu-settings-font.

    Shows a debug tool for the font manager.
    """
    internal_dpg.show_tool(internal_dpg.mvTool_Font)


def menu_settings_style_cb(sender) -> None:
    """Callback from menu-settings-style.

    Shows the standard style editor window.
    """
    # print(f'Menu Item: {sender}')
    internal_dpg.show_tool(internal_dpg.mvTool_Style)
    pass


def menu_settings_theme_cb(sender, app_data, user_data) -> None:
    """Callback from menu-settings-theme-dark.

    Switch theme to dark theme.
    """
    # print(f'Menu Item: {sender}')
    # print(f'App Data: {app_data}')
    # print(f'User Data: {user_data}')
    dpg.bind_theme(user_data)
    pass


def menu_settings_toggle_full_screen_cb() -> None:
    """Callback from menu-settings-toggle full screen.

    Toggle Viewport to full screen and normal mode.
    """
    # print(f'Menu Item: {sender}')
    internal_dpg.toggle_viewport_fullscreen()


def menu_help_metrics_cb() -> None:
    """Callback from menu-help-metrics.

    Shows the standard metrics window.
    """
    internal_dpg.show_tool(internal_dpg.mvTool_Metrics)


def menu_help_help_cb(sender) -> None:
    """Callback from menu-help-help.

    Shows the applications help window.
    """
    # print(f'Menu Item: {sender}')
    try:
        with dpg.window(label='Application Help', modal=True, show=True, tag='modal_help_id', no_title_bar=False):
            with dpg.group(horizontal=False):
                dpg.add_text('This is the help text for this application.')
                dpg.add_text('This script serves as templalte for new GUIs.')
                dpg.add_text('Add/replace the help text here.')
                dpg.add_button(label="Close", width=75, callback=lambda: dpg.configure_item("modal_help_id", show=False))
    except Exception:
        dpg.configure_item("modal_help_id", show=True)
    pass


def menu_help_dpg_cb() -> None:
    """Callback from menu-help-dpg.

    Shows the standard documentation window.
    """
    internal_dpg.show_tool(internal_dpg.mvTool_Doc)


def menu_help_about_me_cb(sender) -> None:
    """Callback from menu-help-about me.

    Shows the about me window.
    """
    # print(f'Menu Item: {sender}')
    try:
        with dpg.window(label='About this Application', modal=True, show=True, tag='modal_about_id', no_title_bar=False):
            with dpg.group(horizontal=False):
                dpg.add_text('This template is meant as starting point for desktop apps using DearPyGui.')
                dpg.add_text(f'DearPyGui Version: {internal_dpg.get_app_configuration()["version"]}')
                dpg.add_text('Lizenziert unter EUPL V. 1.2')
                dpg.add_text("""Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n
eMail: H.Brand@gsi.de\n
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme""")
                dpg.add_button(label="Close", width=75, callback=lambda: dpg.configure_item("modal_about_id", show=False))
    except Exception:
        dpg.configure_item("modal_about_id", show=True)
    pass


def menu_help_about_dpg_cb() -> None:
    """Callback from menu-help-dearpygui.

    Shows the standard about window.
    """
    internal_dpg.show_tool(internal_dpg.mvTool_About)


def viewport_quit_confirmed(sender) -> None:
    """Callback from viewport_quit.

    Quit, dialog was confirmed.
    """
    # print(f'viewport_quit_confirmed: sender: {sender}')
    dpg.configure_item("modal_quit_id", show=False)
    print('Viewport quit confirmed by user.')
    exit(0)
    pass


def viewport_quit(sender) -> None:
    """Callback from menu-file-quit.

    Show dialogue to confirm quit.
    """
    # print(f'Menu Item: {sender}')
    try:
        with dpg.window(label='Quit Application?', modal=True, show=True, tag='modal_quit_id', no_title_bar=False):
            dpg.add_text('Are you sure to quit this application?')
            with dpg.group(horizontal=True):
                dpg.add_button(label="Quit", width=75, callback=viewport_quit_confirmed)
                dpg.add_button(label="Cancel", width=75, callback=lambda: dpg.configure_item("modal_quit_id", show=False))
    except Exception:
        dpg.configure_item("modal_quit_id", show=True)
    pass


def viewport_exit_confirmed(sender) -> None:
    """Callback from viewport_exit.

    Exit, dialog was confirmed.
    """
    # print(f'viewport_exit_confirmed: sender: {sender}')
    dpg.configure_item("modal_exit_id", show=False)
    print('Viewport exit confirmed by user.')
    exit(0)
    pass


def viewport_exit(sender) -> None:
    """Callback from menu-file-exit.

    Show dialogue to confirm exit.
    """
    # print(f'Menu Item: {sender}')
    try:
        with dpg.window(label='Exit Application?', modal=True, show=True, tag='modal_exit_id', no_title_bar=False):
            dpg.add_text('Are you sure to exit this application?')
            with dpg.group(horizontal=True):
                dpg.add_button(label="Exit", width=75, callback=viewport_exit_confirmed)
                dpg.add_button(label="Cancel", width=75, callback=lambda: dpg.configure_item("modal_exit_id", show=False))
    except Exception:
        dpg.configure_item("modal_exit_id", show=True)
    pass


def main():
    """This is the main entry point for this application."""
    dpg.create_context()
    # Create and bint theme
    dark_theme = dpg_themes.create_theme_imgui_dark()
    light_theme = dpg_themes.create_theme_imgui_light()
    dpg.bind_theme(dark_theme)
    session_init_filename = 'dpg_gui_app_session.ini'  # Default session init-file.
    dpg.configure_app(init_file=session_init_filename)
    with dpg.value_registry():  # Register some global variables
        # Possible datatypes:
        # dpg.add_bool_value(default_value=True, tag="bool_value")
        # dpg.add_color_value(default_value=True, tag="color_value")
        # dpg.add_double_value(default_value=0.0, tag="double_value")
        # dpg.add_double4_value(default_value=0.0, tag="double4_value")
        # dpg.add_float_value(default_value=0.0, tag="float_value")
        # dpg.add_float4_value(default_value=0.0, tag="float4_value")
        # dpg.add_int_value(default_value=0, tag="int_value")
        # dpg.add_int4_value(default_value=0, tag="int4_value")
        # dpg.add_series_value(default_value=True, tag="series_value")
        # dpg.add_string_value(default_value="Default string", tag="string_value")
        dpg.add_string_value(default_value="", tag="file_name")
        dpg.add_string_value(default_value="", tag="file_selections")
    with dpg.window(label='Primary Window', tag='Primary Window',
                    width=640, height=460,  # Menu eight is 20 pixels
                    min_size=(320, 240),
                    max_size=(1920, 1080),
                    menubar=False,
                    collapsed=False,
                    autosize=False,
                    no_title_bar=False,
                    no_move=False,
                    no_scrollbar=False,
                    horizontal_scrollbar=True,
                    no_focus_on_appearing=False,
                    no_close=True,
                    no_background=False,
                    no_scroll_with_mouse=False,
                    on_close=None
                    ):
        with dpg.viewport_menu_bar():  # Menu eight is 20 pixels
            with dpg.menu(label="File"):
                dpg.add_menu_item(label="Open", callback=menu_file_dialog_cp)
                dpg.add_menu_item(label="Save", callback=menu_file_save_cb, user_data='Save file requested by user.')
                dpg.add_menu_item(label="Save As", callback=menu_file_dialog_cp)
                dpg.add_separator()
                dpg.add_menu_item(label="Save DPG Session", callback=menu_file_save_session_cb, user_data=session_init_filename)
                dpg.add_separator()
                dpg.add_menu_item(label="Quit", callback=viewport_quit)
                dpg.add_menu_item(label="Exit", callback=viewport_exit)
            with dpg.menu(label="Settings"):
                dpg.add_menu_item(label="Font...", callback=menu_settings_font_cb)
                dpg.add_menu_item(label="Style...", callback=menu_settings_style_cb)
                with dpg.menu(label="Theme"):
                    dpg.add_menu_item(label="Dark", callback=menu_settings_theme_cb, user_data=dark_theme)
                    dpg.add_menu_item(label="Light", callback=menu_settings_theme_cb, user_data=light_theme)
                dpg.add_separator()
                dpg.add_menu_item(label="Toggle full screen", callback=menu_settings_toggle_full_screen_cb)
            with dpg.menu(label="Help"):
                dpg.add_menu_item(label="Help...", callback=menu_help_help_cb)
                dpg.add_menu_item(label="Help DearPyGUI...", callback=menu_help_dpg_cb)
                dpg.add_menu_item(label="Metrics...", callback=menu_help_metrics_cb)
                dpg.add_separator()
                dpg.add_menu_item(label="About me...", callback=menu_help_about_me_cb)
                dpg.add_menu_item(label="About DearPyGUI...", callback=menu_help_about_dpg_cb)
        with dpg.group(pos=[3, 20], indent=0):
            dpg.add_text('This is the primary windows content.', tag='content_title')
            with dpg.tooltip("content_title"):
                dpg.add_text("Content Title")
            dpg.add_text('Replace with application content.', tag='content_text')
            with dpg.tooltip("content_text"):
                dpg.add_text("Content to be replaced.")
    try:
        dpg.create_viewport(title=re.split('.py', os.path.basename(__file__))[0] + ' View Port',
                            x_pos=0, y_pos=0,
                            width=640, height=480,
                            min_width=320, min_height=240,
                            max_width=1920, max_height=1080,
                            resizable=True,
                            always_on_top=False,
                            decorated=True,
                            disable_close=False
                            )
        dpg.set_exit_callback(viewport_quit)
        dpg.setup_dearpygui()
        dpg.show_viewport()
        dpg.set_primary_window('Primary Window', True)
        dpg.start_dearpygui()
        """
        while dpg.is_dearpygui_running():  # replaces, start_dearpygui()
            # do something
            dpg.render_dearpygui_frame()
            pass
        """
    except Exception as e:
        print(e)
    finally:
        dpg.destroy_context()
    pass


if __name__ == '__main__':
    print(os.path.basename(__file__) + "\n\
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\
eMail: H.Brand@gsi.de\n\
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n")
    main()
    print(__name__, 'Done.')
